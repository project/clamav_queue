# ClamAV Queue

## Use case

This module is intended for the very specific case where a site is using all of these modules:

* clamav
* file_entity
* plupload
* multiform

(Note: All these modules are listed as dependencies for this reason.)

## Requirements

* The module also depends on the `advancedqueue` module for its functionality.
* The site must have a "private" file-system configured.

## Purpose

This module displays an optional "Perform virus-scanning in scheduled batches" checkbox when uploading multiple files, so that the editors can choose to "queue" the uploads for virus scanning, rather than waiting them to be scanned immediately. This is meant to avoid timeouts and incomplete uploads that can happen when a very large number of files has to go through the ClamAV process.

## Installation

1. Enable the module as normal.
2. Give the "queue files for clamav processing" permission to the desired role/s.

## Operation

Whenever editors tick the "Perform virus-scanning in scheduled batches" checkbox, the uploads will be automatically saved into the "private" file-system.

The `advancedqueue` module must be used to perform the virus scanning. The easiest approach is to tick the "Process advanced queue via Cron" checkbox at `/admin/config/system/advancedqueue`. After this, virus-scanning will be performed on cron runs.

## Advanced

For a more scalable approach, see the advancedqueue documentation about using Drush to process the queues. For reference, the name of this queue is `clamav_queue`, so the Drush command would be something like:

`drush advancedqueue clamav_queue` (note that this is a looping Drush command)
