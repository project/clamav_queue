<?php

/**
 * @file
 * Module code for ClamAV Queue module.
 */

include_once dirname(__FILE__) . '/clamav_queue.helpers.inc';

/**
 * Implements hook_form_FORM_ID_alter().
 */
function clamav_queue_form_file_entity_add_upload_multiple_alter(&$form, $form_state) {

  if (clamav_queue_allow_new_queue_items()) {
    $form['clamav_queue'] = array(
      '#type' => 'checkbox',
      '#title' => t('Perform virus-scanning in scheduled batches'),
      '#description' => t('If selected, these uploads will complete much more quickly, but will be kept private until virus-scanning completes. Recommended if you are uploading a large number of files.'),
    );
    $form['#submit'][] = '_clamav_queue_file_entity_add_upload_multiple_submit';

    // A round-about way to send a signal to hook_clamav_file_is_scannable().
    if (!empty($form_state['input']['clamav_queue'])) {
      $form['upload']['#upload_validators']['_clamav_queue_flag_file_during_upload'] = array();
    }
  }
}

/**
 * Add a throw-away property to the file object so that we can pass a message.
 */
function _clamav_queue_flag_file_during_upload($file) {
  $file->clamav_queue = TRUE;
}

/**
 * Submit callback for the file_entity_add_upload_multiple form.
 */
function _clamav_queue_file_entity_add_upload_multiple_submit($form, $form_state) {

  // If the user opted to "upload to queue"
  if (!empty($form_state['input']['clamav_queue'])) {
    if (!empty($form_state['files'])) {
      $queue = DrupalQueue::get('clamav_queue', TRUE);
      foreach (array_keys($form_state['files']) as $fid) {
        $task = array(
          'timestamp' => time(),
          // We use "title" for fid because advancedqueue uses this in a
          // dedicated database column, making it easier to lookup later.
          'title' => $fid,
          'uid' => $GLOBALS['user']->uid,
        );
        $queue->createItem($task);

        // Because hook_file_update does not get triggered here, we need to take
        // this opportunity to force the file to be private.
        $file = file_load($fid);
        clamav_queue_force_file_scheme($file, 'private');
      }
    }
  }
}

/**
 * Implements hook_file_update().
 */
function clamav_queue_file_update($file) {

  if (clamav_queue_file_is_queued($file)) {
    // Don't let files become public as long as they're queued to be scanned.
    clamav_queue_force_file_scheme($file, 'private');
  }
}

/**
 * Implements hook_permission().
 */
function clamav_queue_permission() {

  return array(
    'queue files for clamav processing' => array(
      'title' => t('Queue files for ClamAV processing'),
      'description' => t('Use the clamav_queue module to upload files to a queue that will be scanned by ClamAV over time.'),
    ),
  );
}

/**
 * Implements hook_clamav_file_is_scannable().
 */
function clamav_queue_clamav_file_is_scannable($file) {

  // We don't want normal scanning for any files that are queued by this module.
  if (clamav_queue_file_is_queued($file)) {
    return FALSE;
  }

  // This also gets run before the file can be queued, however, during
  // the file_validate() call in plupload_element_validate(). For this case, we
  // rely on a throw-away property that would have been set above in the
  // _clamav_queue_flag_file_during_upload() callback.
  if (isset($file->clamav_queue)) {
    // Cleanup the throw-away property.
    unset($file->clamav_queue);
    // Don't scan this file.
    return FALSE;
  }
}

/**
 * Implements hook_advanced_queue_info().
 */
function clamav_queue_advanced_queue_info() {

  $items['clamav_queue'] = array(
    'label' => t('ClamAV queue'),
    'worker callback' => '_clamav_queue_worker',
  );
  return $items;
}

/**
 * Advanced queue worker callback to process a clamav_queue item.
 *
 * @param $item
 *   The item object to process.
 *
 * @return
 *   An array with "status" and "result" keys.
 */
function _clamav_queue_worker($item) {

  $data = $item->data;
  $file = file_load($data['title']);

  // Log that the process is starting.
  $params = array(
    '@fid' => $file->fid,
    '@time' => date('r', $data['timestamp']),
  );
  advancedqueue_log_message(format_string('The clamav_queue module is now processing file number @fid created at @time.', $params));

  // Assume the best.
  $status = ADVANCEDQUEUE_STATUS_SUCCESS;
  $result = "Scanned {$file->fid} - file was clean.";
  $perform_scan = TRUE;

  // Perform similar checks to what is in clamav_file_validate().
  if (!variable_get('clamav_enabled', TRUE)) {
    $status = ADVANCEDQUEUE_STATUS_FAILURE;
    $result = "Skipped {$file->fid} - scanning is disabled.";
    $perform_scan = FALSE;
  }
  elseif (!clamav_scheme_is_scannable(file_uri_scheme($file->uri))) {
    $status = ADVANCEDQUEUE_STATUS_FAILURE;
    $result = "Skipped {$file->fid} - file scheme not scannable.";
    $perform_scan = FALSE;
  }
  elseif (!file_exists($file->uri)) {
    $status = ADVANCEDQUEUE_STATUS_FAILURE;
    $result = "Skipped {$file->fid} - file does not exist.";
    $perform_scan = FALSE;
  }
  else {
    $modules = module_implements('clamav_file_is_scannable');
    foreach ($modules as $module) {
      // Ignore this module's own implementation of hook_clamav_file_is_scannable.
      if ('clamav_queue' == $module) {
        continue;
      }
      elseif (module_invoke($module, 'clamav_file_is_scannable', $file) === FALSE) {
        $status = ADVANCEDQUEUE_STATUS_FAILURE;
        $result = "Skipped {$file->fid} - $module module prevented scan.";
        $perform_scan = FALSE;
        break;
      }
    }
  }

  // If we are still OK, proceed with the scan.
  if ($perform_scan) {
    module_load_include('inc', 'clamav');
    $scan = clamav_scan_file($file->uri, $file->filename);
    if (CLAMAV_SCANRESULT_INFECTED == $scan) {
      $result = "Scanned {$file->fid} - file was infected.";
    }
    elseif (CLAMAV_SCANRESULT_UNCHECKED == $scan) {
      $result = "Could not scan {$file->fid} - will retry.";
      $status = ADVANCEDQUEUE_STATUS_FAILURE_RETRY;
    }
    elseif (CLAMAV_SCANRESULT_CLEAN == $scan) {
      // Finally, if all went well, make the file public.
      clamav_queue_force_file_scheme($file, 'public');
    }
  }

  return array(
    'status' => $status,
    'result' => $result,
  );
}
