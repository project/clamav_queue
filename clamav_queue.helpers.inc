<?php

/**
 * @file
 * Helper functions for the ClamAV Queue module.
 */

/**
 * Helper function to decide if this module should allow new items in the queue.
 */
function clamav_queue_allow_new_queue_items() {

  $clamav_active = variable_get('clamav_enabled', TRUE);
  $has_permission = user_access('queue files for clamav processing');
  return $clamav_active && $has_permission;
}

/**
 * Helper function to make a file private or public.
 *
 * @param $file
 *   The file object.
 *
 * @param $scheme
 *   The scheme to force, such as 'private' or 'public'.
 */
function clamav_queue_force_file_scheme($file, $scheme) {

  $destination = $scheme . '://' . file_uri_target($file->uri);
  if ($destination != $file->uri) {
    $directory = drupal_dirname($destination);
    if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
      file_move($file, $destination);
    }
  }
}

/**
 * Helper function to see if a file should be scanned.
 *
 * @param $file
 *   The file object.
 */
function clamav_queue_file_is_queued($file) {

  $status = db_select('advancedqueue', 'aq')
    ->fields('aq', array('status'))
    ->condition('name', 'clamav_queue')
    ->condition('title', $file->fid)
    ->execute()
    ->fetchField();

  // Default to assuming that files are not queued.
  $is_queued = FALSE;

  // If the queue item succeeded or failed, it is no longer technically queued.
  if (!empty($status)) {
    $is_queued = !in_array($status, array(
      ADVANCEDQUEUE_STATUS_SUCCESS,
      ADVANCEDQUEUE_STATUS_FAILURE,
    ));
  }

  return $is_queued;
}
